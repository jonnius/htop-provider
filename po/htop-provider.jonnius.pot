# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the htop-provider.jonnius package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: htop-provider.jonnius\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-20 16:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:37 htop-provider.desktop.in.h:1
msgid "HTop"
msgstr ""

#: ../qml/Main.qml:47
msgid ""
"To make htop available in your terminal, run the following command:\n"
"echo 'export PATH=$PATH:/opt/click.ubuntu.com/htop-provider.jonnius/current/"
"bin' >> ~/.bashrc"
msgstr ""
